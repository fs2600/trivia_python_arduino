import requests
import json
import html
import os

import serial
ser = serial.Serial('/dev/ttyUSB0', 115200)

os.system('clear')

url = "https://opentdb.com/api.php?amount=10&category=18&type=multiple"


raw = requests.get(url).text

d= json.loads(raw)



for q in d['results']:
	print(html.unescape(q['question']))
	all_answers =[] 
	all_answers.append(html.unescape(q['correct_answer']))
	for el in q['incorrect_answers']:
		all_answers.append(html.unescape(el))
	print(all_answers)
	
	line = ser.readline()
	print(line)
	if "10" in str(line):
		print("PLAYER 2")
		input()

	if "2" in str(line):
		print("PLAYER 1")
		input()
	
	print(f"Correct: {html.unescape(q['correct_answer'])}")
	print('-----------------------------')